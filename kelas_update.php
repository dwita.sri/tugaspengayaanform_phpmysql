<!-- proses update data  -->

<?php 
    //proses update
    if(isset($_POST['update'])){
        $kelas=$_POST['nama_kelas'];
        $prodi=$_POST['prodi'];
        $fakultas=$_POST['fakultas'];

        // proses update
        $sql = "UPDATE kelas SET nama_kelas='$kelas',prodi='$prodi',fakultas='$fakultas' WHERE id_kelas='$id_kelas'";
        if ($conn->query($sql) === TRUE) {
            header("Location:?page=kelas");
        }
    }

    //menampilkan data di form
    $id_kelas=$_GET['id_kelas'];

    $sql = "SELECT * FROM kelas WHERE id_kelas='$id_kelas'";
    $result = $conn->query($sql);
    $row = $result->fetch_assoc();
?>

<form action="" method="POST">
    <div class="row justify-content-center" >
        <div class="col-sm-6">

            <h3 align="center">UPDATE DATA KELAS</h3><br>
            <div class="form-group">
            <label for="nama_kelas">Kelas</label>
            <input type="text" class="form-control" value="<?php echo $row['nama_kelas']; ?>" name="nama_kelas" maxlength="255" required>
            </div><br>
            <div class="form-group">
            <label for="prodi">Prodi</label>
            <input type="text" class="form-control" value="<?php echo $row['prodi']; ?>" name="prodi" maxlength="255" required>
            </div><br>
            <div class="form-group">
            <label for="fakultas">Fakultas</label>
            <input type="text" class="form-control" value="<?php echo $row['fakultas']; ?>" name="fakultas" maxlength="255" required>
            </div><br>


            <input class="btn btn-primary" type="submit" name="update" value="Update">
            <a class="btn btn-danger" href="?page=kelas">Batal</a>
        </div>
    </div>
</form>

<?php
$conn->close();
?>