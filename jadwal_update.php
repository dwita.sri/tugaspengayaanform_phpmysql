<!-- proses update data  -->

<?php 
    //proses update
    if(isset($_POST['update'])){
        $id_dosen=$_POST['id_dosen'];
        $id_kelas=$_POST['id_kelas'];
        $jadwal=$_POST['jadwal'];
        $makul=$_POST['mata_kuliah'];

        // proses update
        $sql = "UPDATE jadwal_kelas SET id_dosen='$id_dosen',jadwal='$jadwal',mata_kuliah='$makul' WHERE id_jadwal='$id_jadwal'";
        if ($conn->query($sql) === TRUE) {
            header("Location:?page=jadwal");
        }
    }

    //menampilkan data di form
    $id_jadwal=$_GET['id_jadwal'];

    $sql = "SELECT * FROM jadwal_kelas WHERE id_jadwal='$id_jadwal'";
    $result = $conn->query($sql);
    $row = $result->fetch_assoc();
?>

<form action="" method="POST">
    <div class="row justify-content-center" >
        <div class="col-sm-6">

            <h3 align="center">UPDATE DATA JADWAL</h3><br>
            <div class="form-group">
            <label for="jadwal">Jadwal</label>
            <input type="date" class="form-control" value="<?php echo $row['jadwal']; ?>" name="jadwal" maxlength="255" required>
            </div><br>
            <div class="form-group">
            <label for="mata_kuliah">Mata Kuliah</label>
            <input type="text" class="form-control" value="<?php echo $row['mata_kuliah']; ?>" name="mata_kuliah" maxlength="255" required>
            </div><br>



            <input class="btn btn-primary" type="submit" name="update" value="Update">
            <a class="btn btn-danger" href="?page=jadwal">Batal</a>
        </div>
    </div>
</form>

<?php
$conn->close();
?>