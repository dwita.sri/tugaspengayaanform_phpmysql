<h2 align="center">DATA JADWAL</h2>
<a class="btn btn-primary" href="?page=jadwal&action=tambah" style="margin-bottom: 10px;">Tambah</a>
<table class="table table-bordered" id="myTable" >
    <thead>
      <tr>
        <th>Jadwal</th>
        <th>Mata Kuliah</th>
        <th width="120px">Opsi</th>
      </tr>
    </thead>
    <tbody>
	<?php
    $sql = "SELECT*FROM jadwal_kelas";
    $result = $conn->query($sql);
    while($row = $result->fetch_assoc()) {
    ?>

    <tr>
    <td><?php echo $row['jadwal']; ?></td>
    <td><?php echo $row['mata_kuliah']; ?></td>
    <td>
        <a class="btn btn-warning" href="?page=jadwal&action=update&id_jadwal=<?php echo $row['id_jadwal']; ?>">Edit</a>
        <a onclick="return confirm('Yakin menghapus data ini ?')" class="btn btn-danger" href="?page=jadwal&action=hapus&id_jadwal=<?php echo $row['id_jadwal']; ?>">Hapus</a>
    </td>
    </tr>
    <?php
        }
        $conn->close();
    ?>
   </tbody>
</table>