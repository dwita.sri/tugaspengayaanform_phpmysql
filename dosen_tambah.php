<?php

if(isset($_POST['simpan'])){

    $nip=$_POST['nip'];
    $nama=$_POST['nama'];
    $prodi=$_POST['prodi'];
    $fakultas=$_POST['fakultas'];

    $image_foto	= addslashes($_FILES['userfile']['name']);

    $sql = "SELECT*FROM dosen WHERE nip_dosen='$nip'";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
      ?>
        <div class="alert alert-danger d-flex align-items-center" role="alert">
          <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
          <div>NIP Sudah digunakan</div>
        </div>      
      <?php
    }else{
      if($image_foto==''){
          ?>
            <div class="alert alert-danger d-flex align-items-center" role="alert">
              <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
              <div>File gambar belum dipilih</div>
            </div>  
          <?php
      }else{
    
        $ekstensi_diperbolehkan	= array('png','jpg','jpeg');
        $x = explode('.', $image_foto);
        $ekstensi = strtolower(end($x));
      
        $ukuran	= $_FILES['userfile']['size'];
    
        if(in_array($ekstensi, $ekstensi_diperbolehkan) === true){
          if($ukuran == 0){
            ?>
              <div class="alert alert-danger d-flex align-items-center" role="alert">
                <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                <div>Ukuran file gambar terlalu besar</div>
              </div> 
            <?php
          }else if($ukuran < 1000000){
              $image = addslashes(file_get_contents($_FILES['userfile']['tmp_name']));
          
              $sql = "INSERT INTO dosen VALUES (Null,'$image','$nip','$nama','$prodi','$fakultas')";
              if ($conn->query($sql) === TRUE) {
                  header("location:index.php?page=dosen");
              }
          }else{
              ?>
              <div class="alert alert-danger d-flex align-items-center" role="alert">
                <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                <div>Ukuran file gambar terlalu besar</div>
              </div> 
              <?php
          }
        }else{
            ?>
             <div class="alert alert-danger d-flex align-items-center" role="alert">
                <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                <div>Tipe File tidak diperbolehkan</div>
              </div> 
            <?php
        }
      }
    }
}
$conn->close();
?>

            <form action="" name="Form" method="POST" enctype="multipart/form-data">
                <div class="row justify-content-center" >
                    <div class="col-sm-6">

                         <h3 align="center">INPUT DATA JADWAL</h3><br>
                        <div class="form-group">
                            <label for="">NIP  </label>
                            <input type="text" class="form-control mb-2" name="nip" maxlength="20" placeholder="NIP Dosen" required>
                        </div>
                        <div class="form-group">
                            <label for="">Nama Dosen  </label>
                            <input type="text" class="form-control mb-2" name="nama" maxlength="100" placeholder="Nama Dosen" required>
                        </div>
                        <div class="form-group">
                            <label for="">Prodi  </label>
                            <input type="text" class="form-control mb-2" name="prodi" maxlength="100" placeholder="Prodi" required>
                        </div>
                        <div class="form-group mt-2">
                            <label for="">Fakultas  </label>
                            <input type="text" class="form-control mb-2" name="fakultas" maxlength="100" placeholder="Fakultas" required>
                        </div>
                        <div class="form-group">
                        <label>Foto Dosen (Ukuran gambar maksimal 1 MB) </label>
                        <input type="file" class="form-control" name="userfile" id="userfile" onchange="tampilkanPreview(this,'preview')" accept=".jpg,.jpeg,.png">
                            <br><label>Preview Gambar </label><br>
                            <img class="img-thumbnail" id="preview" width="150px" height="225px"/>
                        </div>
                        <div class="mt-3">
                            <input class="btn btn-primary" type="submit" name="simpan" value="Simpan">
                            <a class="btn btn-danger" href="?page=dosen">Batal</a>
                      </div>
                    </div>
                </div>
            </form>
                

<script type="text/javascript">
function tampilkanPreview(userfile,idpreview)
  {
    var gb = userfile.files;
    for (var i = 0; i < gb.length; i++)
    {
      var gbPreview = gb[i];
      var imageType = /image.*/;
      var preview=document.getElementById(idpreview);
      var reader = new FileReader();
      if (gbPreview.type.match(imageType))
      {
        //jika tipe data sesuai
        preview.file = gbPreview;
        reader.onload = (function(element)
        {
          return function(e)
          {
            element.src = e.target.result;
          };
        })(preview);
        //membaca data URL gambar
        reader.readAsDataURL(gbPreview);
      }
    }
  }
</script>
