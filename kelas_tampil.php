<h2 align="center">DATA KELAS</h2>
<a class="btn btn-primary" href="?page=kelas&action=tambah" style="margin-bottom: 10px;">Tambah</a>
<table class="table table-bordered" id="myTable" >
    <thead>
      <tr>
        <th>Kelas</th>
        <th>Prodi</th>
        <th>Fakultas</th>
        <th width="120px">Opsi</th>
      </tr>
    </thead>
    <tbody>
	<?php
    $sql = "SELECT*FROM kelas";
    $result = $conn->query($sql);
    while($row = $result->fetch_assoc()) {
    ?>
    <tr>
    <td><?php echo $row['nama_kelas']; ?></td>
    <td><?php echo $row['prodi']; ?></td>
    <td><?php echo $row['fakultas']; ?></td>
    <td>
        <a class="btn btn-warning" href="?page=kelas&action=update&id_kelas=<?php echo $row['id_kelas']; ?>">Edit</a>
        <a onclick="return confirm('Yakin menghapus data ini ?')" class="btn btn-danger" href="?page=kelas&action=hapus&id_kelas=<?php echo $row['id_kelas']; ?>">Hapus</a>
    </td>
    </tr>
    <?php
        }
        $conn->close();
    ?>
   </tbody>
</table>