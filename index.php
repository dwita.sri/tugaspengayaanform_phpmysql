<?php 
    date_default_timezone_set("Asia/Jakarta");
    include "config.php";
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/jquery.dataTables.min.css">

     <!-- BStyle CSS -->
     <link rel="stylesheet" href="style.css">

    <title></title>
  </head>
  <body>

    <!-- navbar menu -->
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <ul class="navbar-nav">
            <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="?page=dosen">Data Dosen</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="?page=kelas">Data Kelas</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="?page=jadwal">Data Jadwal</a>
            </li>
      </ul>
      </nav>

    <div class="container">

      <?php

        //cek koneksi
        if ($conn->connect_error) {
          ?>
            <div class="alert alert-danger">
                KONEKSI DATABASE GAGAL!
            </div>
          <?php
          exit;
        }      
      
        $page = isset($_GET['page']) ? $_GET['page'] : "";
        $action = isset($_GET['action']) ? $_GET['action'] : "";

        if ($page==""){
            include "welcome.php";
        }elseif ($page=="dosen"){
            if ($action==""){
                include "dosen_tampil.php";
            }elseif ($action=="tambah"){
                include "dosen_tambah.php";
            }elseif ($action=="update"){
                include "dosen_update.php";
            }else{
                include "dosen_hapus.php";
            }
        }elseif ($page=="kelas"){
            if ($action==""){
                include "kelas_tampil.php";
            }elseif ($action=="tambah"){
                include "kelas_tambah.php";
            }elseif ($action=="update"){
                include "kelas_update.php";
            }else{
                include "kelas_hapus.php";
            }
        }elseif ($page=="jadwal"){
            if ($action==""){
                include "jadwal_tampil.php";
            }elseif ($action=="tambah"){
                include "jadwal_tambah.php";
            }elseif ($action=="update"){
                include "jadwal_update.php";
            }else{
                include "jadwal_hapus.php";
            }
        }else{
            include "NAMA_HALAMAN";
        }
      ?>
    </div>
	
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="assets/js/jquery-3.4.1.slim.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery-3.5.1.js"></script>
    <script src="assets/js/jquery.dataTables.min.js"></script>
    <script>
      $(document).ready(function() {
          $('#myTable').DataTable();
      } );
    </script>
  </body>
</html>