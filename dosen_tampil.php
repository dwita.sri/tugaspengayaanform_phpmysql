<h2 align="center">DATA DOSEN</h2>
<a class="btn btn-primary" href="?page=dosen&action=tambah" style="margin-bottom: 10px;">Tambah</a>
<table class="table table-bordered" id="myTable" >
    <thead>
      <tr>
        <th>NIP Dosen</th>
        <th>Nama Dosen</th>
        <th>Prodi</th>
        <th>Fakultas</th>
        <th>Foto Dosen</th>
        <th width="120px">Opsi</th>
      </tr>
    </thead>
    <tbody>
        <?php
          $sql = "SELECT*FROM dosen ORDER BY nip_dosen ASC";
          $result = $conn->query($sql);
          while($row = $result->fetch_assoc()) {
          ?>
            <tr>
              <?php $gambare=base64_encode($row['foto_dosen']); ?>
              <td><?php echo $row['nip_dosen']; ?></td>
              <td><?php echo $row['nama_dosen']; ?></td>
              <td><?php echo $row['prodi']; ?></td>
              <td><?php echo $row['fakultas']; ?></td>
              <td><img class="img-thumbnail" id="preview" src="data:image/jpg;base64, <?php echo $gambare; ?>" width="100px" height="90px"/></td>
              <td>
                  <a class="btn btn-warning" href="?page=dosen&action=update&id_dosen=<?php echo $row['id_dosen']; ?>">Edit</a>
                  <a onclick="return confirm('Yakin menghapus data ini ?')" class="btn btn-danger" href="?page=dosen&action=hapus&id_dosen=<?php echo $row['id_dosen']; ?>">Hapus</a>
              </td>
            </tr>
          <?php
              }
              $conn->close();
          ?>
   </tbody>
</table>