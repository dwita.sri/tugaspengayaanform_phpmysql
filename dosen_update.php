<?php
$id=$_GET['id'];

$sql = "SELECT*FROM dosen WHERE id_dosen='$id'";
$result = $conn->query($sql);
$row = $result->fetch_assoc();

$gambare=base64_encode($row['foto_dosen']);
$nipe=$row['nip_dosen'];

if(isset($_POST['update'])){
    $nip=$_POST['nip'];
    $nama=$_POST['nama'];
    $prodi=$_POST['prodi'];
    $fakultas=$_POST['fakultas'];

    $image_foto	= addslashes($_FILES['userfile']['name']);

    if($nipe!=$nip){
      $sql = "SELECT*FROM dosen WHERE nip_dosen='$nip'";
      $result = $conn->query($sql);
      if ($result->num_rows > 0) {
        ?>
        <div class="alert alert-danger d-flex align-items-center" role="alert">
          <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
          <div>NIP Sudah digunakan</div>
        </div>     
        <?php
      }else{
        goto simpan;
      }
    }else{
      simpan:
      if ($_FILES['userfile']['tmp_name']=='') {
        $sql = "UPDATE dosen SET nip_dosen='$nip',nama_dosen='$nama',prodi='$prodi',fakultas='$fakultas' WHERE id_dosen='$id'";
        if ($conn->query($sql) === TRUE) {
            header("location:index.php?page=dosen");
        }
      }else{
        if($image_foto==''){
            ?>
              <div class="alert alert-danger d-flex align-items-center" role="alert">
                <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                <div>
                  File gambar belum dipilih
                </div>
              </div>     
            <?php
        }else{
          $ekstensi_diperbolehkan	= array('png','jpg','jpeg');
          $x = explode('.', $image_foto);
          $ekstensi = strtolower(end($x));
        
          $ukuran	= $_FILES['userfile']['size'];
    
          if(in_array($ekstensi, $ekstensi_diperbolehkan) === true){
            if($ukuran == 0){
              ?>
                <div class="alert alert-danger d-flex align-items-center" role="alert">
                  <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                  <div>
                    Ukuran file terlalu besar
                  </div>
                </div>
              <?php
            }else if($ukuran < 1000000){
              $image = addslashes(file_get_contents($_FILES['userfile']['tmp_name']));
          
              $sql = "UPDATE dosen SET nip_dosen='$nip',nama_dosen='$nama',prodi='$prodi',fakultas='$fakultas',foto_dosen='$image' WHERE id_dosen='$id'";
              if ($conn->query($sql) === TRUE) {
                  header("location:index.php?page=dosen");
              }
            }else{
              ?>
                <div class="alert alert-danger d-flex align-items-center" role="alert">
                  <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                  <div>
                    Ukuran file terlalu besar
                  </div>
                </div>            
              <?php
            }
          }else{
            ?>
              <div class="alert alert-danger d-flex align-items-center" role="alert">
                <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                <div>
                  Tipe File tidak diperbolehkan
                </div>
              </div>
            <?php
          }
        }
      }
    }
  }

?>

            <form action="" name="Form" method="POST" enctype="multipart/form-data">
                <div class="row justify-content-center" >
                    <div class="col-sm-6">

                        <h3 align="center">UPDATE DATA DOSEN</h3><br>
                        <div class="form-group">
                            <label for="">NIP Dosen : </label>
                            <input type="text" class="form-control mb-2" name="nip" value="<?php echo $row['nip_dosen']; ?>" maxlength="20" placeholder="NIP Dosen" required>
                        </div>
                        <div class="form-group">
                            <label for="">Nama Dosen : </label>
                            <input type="text" class="form-control mb-2" name="nama" value="<?php echo $row['nama_dosen']; ?>" maxlength="100" placeholder="Nama Dosen" required>
                        </div>
                        <div class="form-group">
                            <label for="">Prodi : </label>
                            <input type="text" class="form-control mb-2" name="prodi" value="<?php echo $row['prodi']; ?>" maxlength="100" placeholder="Prodi" required>
                        </div>
                        <div class="form-group">
                            <label for="">Fakultas : </label>
                            <input type="text" class="form-control mb-2" name="fakultas" value="<?php echo $row['fakultas']; ?>" maxlength="100" placeholder="Fakultas" required>
                        </div>
                        <div class="form-group">
                            <label>Foto Dosen (Ukuran gambar maksimal 1 MB) :</label>
                            <input type="file" class="form-control" name="userfile" id="userfile" onchange="tampilkanPreview(this,'preview')" accept=".png,.jpg,.jpeg">
                            <br><label>Preview Gambar :</label><br>
                            <img class="img-thumbnail" id="preview" src="data:image/jpg;base64, <?php echo $gambare; ?>" width="150px" height="225px"/>
                        </div>     
                        <div class="mt-3">
                            <input class="btn btn-primary" type="submit" name="update" value="Update">
                            <a class="btn btn-danger" href="?page=dosen">Batal</a>
                        </div>
                    </div>
                </div>
            </form>

<?php 
$conn->close();
?>

<script type="text/javascript">
function tampilkanPreview(userfile,idpreview)
  {
    var gb = userfile.files;
    for (var i = 0; i < gb.length; i++)
    {
      var gbPreview = gb[i];
      var imageType = /image.*/;
      var preview=document.getElementById(idpreview);
      var reader = new FileReader();
      if (gbPreview.type.match(imageType))
      {
        //jika tipe data sesuai
        preview.file = gbPreview;
        reader.onload = (function(element)
        {
          return function(e)
          {
            element.src = e.target.result;
          };
        })(preview);
        //membaca data URL gambar
        reader.readAsDataURL(gbPreview);
      }
    }
  }
</script>
